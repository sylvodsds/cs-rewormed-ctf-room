var gameSettings = new Map();

gameSettings.set("ctf",
{
    scoreLimit: 5,
    timeLimit: 10,
    loadingTimes: 0.4,
    gameMode: "ctf",
    levelPool: "arenasBest",
    respawnDelay: 3,
    bonusDrops: "healthAndWeapons",
    maxDuplicateWeapons: 0,
    bonusSpawnFrequency: 6,
    teamsLocked: false,
    weaponChangeDelay: 0,
    forceRandomizeWeapons: true,
    damageMultiplier: 1,
    weapons: {
        banById: [5,30,34,35]
    }
}
);

