#!/bin/bash
trap ' ' INT
if [[ -f "../src/dist/cli.js" ]]
then
    WLEXEC="../src/dist/cli.js"
else
    WLEXEC="wlhl"
fi
echo $WLEXEC

if [[ $3 == "reload" ]]; then
    wget https://www.vgm-quiz.com/dev/webliero/wledit/wlkit.js -O _wlkit.js
    wget https://www.vgm-quiz.com/dev/webliero/wledit/hjson.min.js -O _hjson.js
fi
ROOMID="CS_CTF"
$WLEXEC stop $ROOMID
$WLEXEC launch --id $ROOMID --token $2
$WLEXEC run $ROOMID ./_conf.js
find $1 -maxdepth 1 -type f -name '*.js' -not -name 'mapsettings*' -not -name 'gamesettings*' -not -name '_conf*' | LC_COLLATE=C sort | xargs $WLEXEC run $ROOMID
$WLEXEC run $ROOMID ./gamesettings.js
$WLEXEC run $ROOMID ./mapsettings.js
